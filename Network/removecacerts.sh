#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/gallery/msp \
    ./organizations/fabric-ca/gallery/ca-cert.pem \
    ./organizations/fabric-ca/gallery/fabric-ca-server.db \
    ./organizations/fabric-ca/gallery/IssuerPublicKey \
    ./organizations/fabric-ca/gallery/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/gallery/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/artist/msp \
    ./organizations/fabric-ca/artist/ca-cert.pem \
    ./organizations/fabric-ca/artist/fabric-ca-server.db \
    ./organizations/fabric-ca/artist/IssuerPublicKey \
    ./organizations/fabric-ca/artist/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/artist/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/buyer/msp \
    ./organizations/fabric-ca/buyer/ca-cert.pem \
    ./organizations/fabric-ca/buyer/fabric-ca-server.db \
    ./organizations/fabric-ca/buyer/IssuerPublicKey \
    ./organizations/fabric-ca/buyer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/buyer/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/paint.com/orderer.paint.com/msp \
    ./organizations/ordererOrganizations/paint.com/orderer.paint.com/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers \
    ./organizations/ordererOrganizations/paint.com/orderer.paint.com/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/ca \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/msp \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/peers \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/tlsca \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/users \
    ./organizations/peerOrganizations/paint.com/artist.paint.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/ca \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/msp \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/peers \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/tlsca \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/users \
    ./organizations/peerOrganizations/paint.com/gallery.paint.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/ca \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/msp \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/peers \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/tlsca \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/users \
    ./organizations/peerOrganizations/paint.com/buyer.paint.com/fabric-ca-client-config.yaml