#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${ORGMSP}/$6/" \
        ccp-template.json
}

ORG=artist
P0PORT=7051
CAPORT=7054
PEERPEM=../organizations/peerOrganizations/paint.com/artist.paint.com/tlsca/tls-localhost-7054-ca-artist.pem
CAPEM=../organizations/peerOrganizations/paint.com/artist.paint.com/ca/localhost-7054-ca-artist.pem
ORGMSP=Artist

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-artist.json

ORG=gallery
P0PORT=9051
CAPORT=8054
PEERPEM=../organizations/peerOrganizations/paint.com/gallery.paint.com/tlsca/tls-localhost-8054-ca-gallery.pem
CAPEM=../organizations/peerOrganizations/paint.com/gallery.paint.com/ca/localhost-8054-ca-gallery.pem
ORGMSP=Gallery

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-gallery.json

ORG=buyer
P0PORT=11051
CAPORT=9054
PEERPEM=../organizations/peerOrganizations/paint.com/buyer.paint.com/tlsca/tls-localhost-9054-ca-buyer.pem
CAPEM=../organizations/peerOrganizations/paint.com/buyer.paint.com/ca/localhost-9054-ca-buyer.pem
ORGMSP=Buyer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-buyer.json
