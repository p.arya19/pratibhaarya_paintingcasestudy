sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=autochannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=fabricscratch-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile  AutoChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.paint.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Artist Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.artist.paint.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Gallery Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GalleryMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.gallery.paint.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/users/Admin@gallery.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining MVD Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BuyerMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.buyer.paint.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/users/Admin@buyer.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Generating anchor peer tx for artist ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/ArtistMSPanchors.tx -channelID autochannel -asOrg ArtistMSP

sleep 2

echo "########## Generating anchor peer tx for gallery ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/GalleryMSPanchors.tx -channelID autochannel -asOrg GalleryMSP

sleep 2

echo "########## Generating anchor peer tx for gallery ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/BuyerMSPanchors.tx -channelID autochannel -asOrg BuyerMSP

sleep 2

echo "########## Anchor Peer Update for Artist ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.artist.paint.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.paint.com:7050 -c autochannel -f ./config/ArtistMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Gallery ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GalleryMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.gallery.paint.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/users/Admin@gallery.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.paint.com:7050 -c autochannel -f ./config/GalleryMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Buyer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BuyerMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.buyer.paint.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/users/Admin@buyer.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.paint.com:7050 -c autochannel -f ./config/BuyerMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.artist.paint.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package kbapaint.tar.gz --path /opt/gopath/src/github.com/chaincode/KBA-paint/ --lang node --label kbapaint_1

sleep 2

echo "##########  Install Chaincode on Artist peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ArtistMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.artist.paint.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbapaint.tar.gz


sleep 2

echo "##########  Install Chaincode on Gallery peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GalleryMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.gallery.paint.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/gallery.paint.com/users/Admin@gallery.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbapaint.tar.gz

sleep 2

echo "##########  Install Chaincode on Buyer peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BuyerMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.buyer.paint.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/buyer.paint.com/users/Admin@buyer.paint.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbapaint.tar.gz


echo "##########  Copy the above package ID for next steps, follow the approveCommit.txt ############"
