#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
version: '2'

networks:
  paint:

services:
  couchdbArtistPeer0:
    container_name: couchdbArtistPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Artist
      - COUCHDB_PASSWORD=password
    ports:
      - 5984:5984
    networks:
      - paint

  couchdbGalleryPeer0:
    container_name: couchdbGalleryPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Gallery
      - COUCHDB_PASSWORD=password
    ports:
      - 7984:5984
    networks:
      - paint

  couchdbBuyerPeer0:
    container_name: couchdbBuyerPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Buyer
      - COUCHDB_PASSWORD=password
    ports:
      - 9984:5984
    networks:
      - paint

  orderer.paint.com:
    container_name: orderer.paint.com
    image: hyperledger/fabric-orderer:2.2.3
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_LISTENPORT=7050
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
    command: orderer
    ports:
      - 7050:7050
    volumes:
      - ../channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ../organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp:/var/hyperledger/orderer/msp
      - ../organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls:/var/hyperledger/orderer/tls
    networks:
      - paint

  peer0.artist.paint.com:
    container_name: peer0.artist.paint.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.artist.paint.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.artist.paint.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.artist.paint.com:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.artist.paint.com:7051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=ArtistMSP
      - CORE_PEER_ADDRESS=peer0.artist.paint.com:7051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_paint
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbArtistPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Artist
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 7051:7051
      - 7053:7053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.paint.com
      - couchdbArtistPeer0  
    networks:
      - paint

  peer0.gallery.paint.com:
    container_name: peer0.gallery.paint.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.gallery.paint.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:9051
      - CORE_PEER_CHAINCODEADDRESS=peer0.gallery.paint.com:9052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:9052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.gallery.paint.com:9051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.gallery.paint.com:9051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=GalleryMSP
      - CORE_PEER_ADDRESS=peer0.gallery.paint.com:9051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_paint
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbGalleryPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Gallery
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 9051:9051
      - 9053:9053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.paint.com
      - couchdbGalleryPeer0  
    networks:
      - paint

  peer0.buyer.paint.com:
    container_name: peer0.buyer.paint.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.buyer.paint.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:11051
      - CORE_PEER_CHAINCODEADDRESS=peer0.buyer.paint.com:11052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:11052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.buyer.paint.com:11051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.buyer.paint.com:11051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=BuyerMSP
      - CORE_PEER_ADDRESS=peer0.buyer.paint.com:11051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_paint
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbBuyerPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Buyer
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 11051:11051
      - 11053:11053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.paint.com
      - couchdbBuyerPeer0  
    networks:
      - paint   

  cli:
    container_name: cli
    image: hyperledger/fabric-tools:2.2.3
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.artist.paint.com:7051
      - CORE_PEER_LOCALMSPID=ArtistMSP
      - CORE_CHAINCODE_KEEPALIVE=10
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp
      - ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ../../Chaincode/:/opt/gopath/src/github.com/chaincode/
      - ../organizations:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ../channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/config/
    networks:
        - paint
    depends_on:
      - orderer.paint.com
      - peer0.artist.paint.com
      - peer0.gallery.paint.com
      - peer0.buyer.paint.com

