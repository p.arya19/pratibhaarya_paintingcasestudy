echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/paint.com/artist.paint.com/
mkdir -p organizations/peerOrganizations/paint.com/gallery.paint.com/
mkdir -p organizations/peerOrganizations/paint.com/buyer.paint.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/
sleep 2

echo "########## Enroll Artist ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-artist --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem

sleep 5

echo "########## Create config.yaml for Artist ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-artist.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/msp/config.yaml

echo "########## Register artist peer ############"
fabric-ca-client register --caname ca-artist --id.name peer0artist --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Register artist user ############"

fabric-ca-client register --caname ca-artist --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Register artist Org admin ############"

fabric-ca-client register --caname ca-artist --id.name artistadmin --id.secret artistadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Generate Artist peer MSP ############"

fabric-ca-client enroll -u https://peer0artist:peer0pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/msp --csr.hosts peer0.artist.paint.com --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Generate Artist Peer tls cert ############"

fabric-ca-client enroll -u https://peer0artist:peer0pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls --enrollment.profile tls --csr.hosts peer0.artist.paint.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/tlscacerts/* organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/ca.crt
cp organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/signcerts/* organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.crt
cp organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/keystore/* organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/tlsca
cp ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/tlsca/tls-localhost-7054-ca-artist.pem

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/ca
cp ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/ca/localhost-7054-ca-artist.pem

cp organizations/peerOrganizations/paint.com/artist.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/artist.paint.com/peers/peer0.artist.paint.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/users/User1@artist.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/paint.com/artist.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/artist.paint.com/users/User1@artist.paint.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://artistadmin:artistadminpw@localhost:7054 --caname ca-artist -M ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/artist/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/paint.com/artist.paint.com/users/Admin@artist.paint.com/msp/config.yaml

echo "============================================================End of Artist =================================================================================================="

echo "================================================================== Gallery =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/

echo "enroll Gallery ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-gallery --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem

sleep 5




echo "Create config.yaml for Gallery" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-gallery.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-gallery.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-gallery.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-gallery.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/msp/config.yaml

echo "Register gallery peer"
echo "=========================="
fabric-ca-client register --caname ca-gallery --id.name peer0gallery --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

echo "Register gallery user"
echo "=========================="
fabric-ca-client register --caname ca-gallery --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

echo "Register gallery Org admin"
echo "=========================="
fabric-ca-client register --caname ca-gallery --id.name galleryadmin --id.secret galleryadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

echo "Generate Gallery peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0gallery:peer0pw@localhost:8054 --caname ca-gallery -M ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/msp --csr.hosts peer0.gallery.paint.com --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

echo "Generate Gallery Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0gallery:peer0pw@localhost:8054 --caname ca-gallery -M ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls --enrollment.profile tls --csr.hosts peer0.gallery.paint.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/tlscacerts/* organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/ca.crt
cp organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/signcerts/* organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.crt
cp organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/keystore/* organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/tlsca
cp ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/tlsca/tls-localhost-8054-ca-gallery.pem

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/ca
cp ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/ca/localhost-8054-ca-gallery.pem

cp organizations/peerOrganizations/paint.com/gallery.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/gallery.paint.com/peers/peer0.gallery.paint.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-gallery -M ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/users/User1@gallery.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/paint.com/gallery.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/gallery.paint.com/users/User1@gallery.paint.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://galleryadmin:galleryadminpw@localhost:8054 --caname ca-gallery -M ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/users/Admin@gallery.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/gallery/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/paint.com/gallery.paint.com/users/Admin@gallery.paint.com/msp/config.yaml


echo "==========================================================End of Gallery ========================================================================================================"

echo "========================================================== Buyer ================================================================================================================="
echo "enroll Buyer ca "
echo "======================"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-buyer --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5





echo "Create config.yaml for Buyer"
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-buyer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-buyer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-buyer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-buyer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/msp/config.yaml

echo "Register buyer peer"
echo "=========================="
fabric-ca-client register --caname ca-buyer --id.name peer0buyer --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

echo "Register buyer user"
echo "=========================="
fabric-ca-client register --caname ca-buyer --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

echo "Register buyer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-buyer --id.name buyeradmin --id.secret buyeradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

echo "Generate Buyer peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0buyer:peer0pw@localhost:9054 --caname ca-buyer -M ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/msp --csr.hosts peer0.buyer.paint.com --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

echo "Generate Buyer Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0buyer:peer0pw@localhost:9054 --caname ca-buyer -M ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls --enrollment.profile tls --csr.hosts peer0.buyer.paint.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5


echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/tlscacerts/* organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/ca.crt
cp organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/signcerts/* organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.crt
cp organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/keystore/* organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/tlsca
cp ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/tlsca/tls-localhost-9054-ca-buyer.pem

mkdir -p ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/ca
cp ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/ca/localhost-9054-ca-buyer.pem

cp organizations/peerOrganizations/paint.com/buyer.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/buyer.paint.com/peers/peer0.buyer.paint.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:9054 --caname ca-buyer -M ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/users/User1@buyer.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/paint.com/buyer.paint.com/msp/config.yaml organizations/peerOrganizations/paint.com/buyer.paint.com/users/User1@buyer.paint.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://buyeradmin:buyeradminpw@localhost:9054 --caname ca-buyer -M ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/users/Admin@buyer.paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/paint.com/buyer.paint.com/users/Admin@buyer.paint.com/msp/config.yaml

echo "==========================================================End of Buyer ========================================================================================================================"

echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/paint.com/orderer.paint.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Buyer "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp --csr.hosts orderer.paint.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls --enrollment.profile tls --csr.hosts orderer.paint.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/tlscacerts/tlsca.paint.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/orderers/orderer.paint.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/users/Admin@paint.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/paint.com/orderer.paint.com/users/Admin@paint.com/msp/config.yaml

echo "=========================================================End of Buyer ========================================================================================================================"







