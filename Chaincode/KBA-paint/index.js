/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const PaintingContract = require('./lib/painting-contract');
const OrderContract = require('./lib/order-contract');

module.exports.PaintingContract = PaintingContract;
module.exports.OrderContract = OrderContract;
module.exports.contracts = [PaintingContract, OrderContract];
