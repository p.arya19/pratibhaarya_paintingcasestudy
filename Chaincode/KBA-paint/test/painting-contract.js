/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { PaintingContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {
    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon
                .stub()
                .returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }
}

describe('PaintingContract', () => {
    let contract;
    let ctx;

    beforeEach(() => {
        contract = new PaintingContract();
        ctx = new TestContext();
        ctx.stub.getState
            .withArgs('1001')
            .resolves(
                Buffer.from(
                    '{"make":"oil painting","model":"X1","paintingName":"lion","dateOfPainting":"10-10-2020","status":"with artist","ownedBy":"Artist-01","assetType":"painting"}'
                )
            );
        ctx.stub.getState
            .withArgs('1002')
            .resolves(
                Buffer.from(
                    '{"make":"modern art","model":"X1","paintingName":"Red","dateOfPainting":"10-10-2020","status":"with artist","ownedBy":"Artist-01","assetType":"painting"}'
                )
            );

        ctx.clientIdentity = {
            getMSPID: function () {
                return 'ArtistMSP';
            },
        };
    });

    describe('#paintingExists', () => {
        it('should return true for a painting', async () => {
            await contract.paintingExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a painting that does not exist', async () => {
            await contract.paintingExists(ctx, '1003').should.eventually.be.false;
        });
    });

    describe('#createPainting', () => {
        it('should create a painting', async () => {
            await contract.createPainting(
                ctx,
                '1003',
                'oil paintiing',
                'X1',
                'lion',
                '10-10-2020',
                'Artist-01'
            );
            ctx.stub.putState.should.have.been.calledOnceWithExactly(
                '1003',
                Buffer.from(
                    '{"make":"oil painting","model":"X1","paintingName":"lion","dateOfPainting":"10-10-2020","status":"with artist","ownedBy":"Artist-01","assetType":"painting"}'

                    
                )
            );
        });

        it('should throw an error for a painting that already exists', async () => {
            await contract
                .createPainting(
                    ctx,
                    '1001',
                    'oil painting',
                    'X1',
                    'lion',
                    '10-10-2020',
                    'Artist-01'
                )
                .should.be.rejectedWith(/The painting 1001 already exists/);
        });
    });

    describe('#readPainting', () => {
        it('should return a painting', async () => {
            await contract.readPainting(ctx, '1001').should.eventually.deep.equal({
                make: 'oil painting',
                model: 'X1',
                paintingName: 'lion',
                dateOfPainting: '10-10-2020',
                status: 'with artist',
                ownedBy: 'Artist-01',
                assetType: 'painting',
            });
        });

        it('should throw an error for a painting that does not exist', async () => {
            await contract
                .readPainting(ctx, '1003')
                .should.be.rejectedWith(/The painting 1003 does not exist/);
        });
    });

    describe('#deletePainting', () => {
        it('should delete a painting', async () => {
            await contract.deletePainting(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a painting that does not exist', async () => {
            await contract
                .deletePainting(ctx, '1003')
                .should.be.rejectedWith(/The painting 1003 does not exist/);
        });
    });
});
