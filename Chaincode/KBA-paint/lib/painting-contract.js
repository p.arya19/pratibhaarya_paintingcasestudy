/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const OrderContract = require('./order-contract');

class PaintingContract extends Contract {
    async paintingExists(ctx, paintingId) {
        const buffer = await ctx.stub.getState(paintingId);
        return !!buffer && buffer.length > 0;
    }

    async createPainting(
        ctx,
        paintingId,
        make,
        model,
        paintingName,
        dateOfPainting,
        artistName
    ) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'ArtistMSP') {
            const exists = await this.paintingExists(ctx, paintingId);
            if (exists) {
                throw new Error(`The painting ${paintingId} already exists`);
            }
            const paintingAsset = {
                make,
                model,
                paintingName,
                dateOfPainting,
                status: 'With Artist',
                ownedBy: artistName,
                assetType: 'painting',
            };
            const buffer = Buffer.from(JSON.stringify(paintingAsset));
            await ctx.stub.putState(paintingId, buffer);
            
            let addPaintingEventData = { Type: 'Painting creation', Model: model };
            await ctx.stub.setEvent('addPaintingEvent', Buffer.from(JSON.stringify(addPaintingEventData)));

        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async readPainting(ctx, paintingId) {
        const exists = await this.paintingExists(ctx, paintingId);
        if (!exists) {
            throw new Error(`The painting ${paintingId} does not exist`);
        }
        const buffer = await ctx.stub.getState(paintingId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async deletePainting(ctx, paintingId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'ArtistMSP') {
            const exists = await this.paintingExists(ctx, paintingId);
            if (!exists) {
                throw new Error(`The painting ${paintingId} does not exist`);
            }
            await ctx.stub.deleteState(paintingId);
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async checkMatchingOrders(ctx, paintingId) {
        const exists = await this.paintingExists(ctx, paintingId);
        if (!exists) {
            throw new Error(`The painting ${paintingId} does not exist`);
        }

        const paintingBuffer = await ctx.stub.getState(paintingId);
        const paintingDetails = JSON.parse(paintingBuffer.toString());

        const queryString = {
            selector: {
                assetType: 'order',
                make: paintingDetails.make,
                model: paintingDetails.model,
                paintingName: paintingDetails.paintingName,
            },
        };

        const orderContract = new OrderContract();
        const orders = await orderContract.queryAllOrders(
            ctx,
            JSON.stringify(queryString)
        );

        return orders;
    }

    async matchOrder(ctx, paintingId, orderId) {
        const orderContract = new OrderContract();

        const paintingDetails = await this.readPainting(ctx, paintingId);
        const orderDetails = await orderContract.readOrder(ctx, orderId);

        if (
            orderDetails.make === paintingDetails.make &&
      orderDetails.model === paintingDetails.model &&
      orderDetails.paintingName === paintingDetails.paintingName
        ) {
            paintingDetails.ownedBy = orderDetails.galleryName;
            paintingDetails.status = 'Assigned to a Gallery';

            const newPaintingBuffer = Buffer.from(JSON.stringify(paintingDetails));
            await ctx.stub.putState(paintingId, newPaintingBuffer);

            await orderContract.deleteOrder(ctx, orderId);
            return `Painting ${paintingId} is assigned to ${orderDetails.galleryName}`;
        } else {
            return 'Order is not matching';
        }
    }

    async registerPainting(ctx, paintingId, buyerName, auctionNumber) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'MvdMSP') {
            const exists = await this.paintingExists(ctx, paintingId);
            if (!exists) {
                throw new Error(`The painting ${paintingId} does not exist`);
            }

            const paintingBuffer = await ctx.stub.getState(paintingId);
            const paintingDetails = JSON.parse(paintingBuffer.toString());

            paintingDetails.status = `Registered to ${buyerName} wih auction number ${auctionNumber}`;
            paintingDetails.ownedBy = buyerName;

            const newPaintingBuffer = Buffer.from(JSON.stringify(paintingDetails));
            await ctx.stub.putState(paintingId, newPaintingBuffer);

            return `Painting ${paintingId} is successfully registered to ${buyerName}`;
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async queryAllPaintings(ctx) {
        const queryString = {
            selector: {
                assetType: 'painting',
            },
            sort: [{ dateOfPainting: 'asc' }],
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getPaintingsByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getPaintingsWithPagination(ctx, _pageSize, _bookmark) {
        const queryString = {
            selector: {
                assetType: 'painting',
            },
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(
            JSON.stringify(queryString),
            pageSize,
            bookmark
        );

        const result = await this.getAllResults(iterator, false);

        const results = {};
        results.Result = result;
        results.ResponseMetaData = {
            RecordCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };
        return JSON.stringify(results);
    }

    async getPaintingsHistory(ctx, paintingId) {
        let resultsIterator = await ctx.stub.getHistoryForKey(paintingId);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());
                } else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes);
            }
        }
        await iterator.close();
        return allResult;
    }
}

module.exports = PaintingContract;
